<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingsRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cleaningType;

    /**
     * @ORM\Column(type="integer")
     */
    private $objectSize;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $bookingDate;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Session", mappedBy="booking", cascade={"persist", "remove"})
     */
    private $sessions;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bookings")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $review;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isGood;


    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->setCreatedAt(new DateTime());
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCleaningType(): ?string
    {
        return $this->cleaningType;
    }

    public function setCleaningType(string $cleaningType): self
    {
        $this->cleaningType = $cleaningType;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getBookingDate(): ?\DateTimeInterface
    {
        return $this->bookingDate;
    }

    public function setBookingDate(\DateTimeInterface $bookingDate): self
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }

    public function getSessions()
    {
        return $this->sessions;
    }

    public function setSessions(string $sessions): self
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * @param mixed $objectSize
     * @return Booking
     */
    public function setObjectSize($objectSize)
    {
        $this->objectSize = $objectSize;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjectSize()
    {
        return $this->objectSize;
    }

    /**
     * @param $user
     * @return Booking
     */
    public function setUser($user): Booking
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $review
     * @return Booking
     */
    public function setReview($review)
    {
        $this->review = $review;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return str_repeat("<i class='far fa-star'></i>", $this->review);
        // return $this->review;
    }

    /**
     * @param mixed $isGood
     * @return Booking
     */
    public function setIsGood($isGood)
    {
        $this->isGood = $isGood;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisGood()
    {
        return $this->isGood;
    }

}
