<?php


// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isTrusted;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="user", cascade={"persist", "remove"})
     */
    private $bookings;

    private $newPass;

    public function __construct()
    {
        parent::__construct();
        $this->setIsTrusted(true);
        $this->bookings = new ArrayCollection();
        // your own logic
    }


    public function getNewPass()
    {
        return $this->newPass;
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;
        return $this;
    }

    /**
     * @param bool $isTrusted
     * @return User
     */
    public function setIsTrusted($isTrusted)
    {
        $this->isTrusted = $isTrusted;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTrusted()
    {
        return $this->isTrusted;
    }

    /**
     * @param ArrayCollection $bookings
     * @return User
     */
    public function setBookings(ArrayCollection $bookings): User
    {
        $this->bookings = $bookings;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBookings(): ArrayCollection
    {
        return $this->bookings;
    }


}