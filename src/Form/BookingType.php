<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', TextType::class, [
                'label' => 'Адрес помещения'
            ])
            ->add('objectSize', NumberType::class, [
                'label' => 'Квадратура помешения'
            ])
            ->add('bookingDate', DateTimeType::class, [
                'label' => 'Время бронирования',
                'years' => range(date('Y'), date('Y') + 5),
                'months' => range(1, 12),
                'days' => range(1, 31),
                'hours' => range(8, 20)
            ])
            ->add('cleaningType', ChoiceType::class, array(
                'label' => 'Кто Вы?',
                'choices' => [
                    'Сухая уборка ' => '1',
                    'Влажная уборка' => '1.5',
                    'Генеральная уборка' => '2.5'],
                'placeholder' => 'Выбери один из вариантов'
            ))
            ->add('Забронировать', SubmitType::class);
    }

    public function getBlockPrefix()
    {
        return 'app_booking_add';
    }
}
