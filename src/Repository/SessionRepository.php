<?php

namespace App\Repository;

use App\Entity\Session;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Session|null find($id, $lockMode = null, $lockVersion = null)
 * @method Session|null findOneBy(array $criteria, array $orderBy = null)
 * @method Session[]    findAll()
 * @method Session[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Session::class);
    }

    public function getSessionByDateAndEmploye($startTime, $endTime, $employe)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->innerJoin('s.employe', 'e')
            ->where('s.endTime > :startTime')
            ->setParameter('startTime', $startTime)
            ->andWhere('s.startTime < :endTime')
            ->setParameter('endTime', $endTime)
            ->andWhere('s.employe = :employe')
            ->setParameter('employe', $employe)
            ->orderBy('s.endTime', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getEmployeWorks($employe)
    {
        return $this->createQueryBuilder('s')
            ->select('COUNT(b)')
            ->innerJoin('s.booking', 'b')
            ->innerJoin('s.employe', 'e')
            ->andWhere('s.employe = :employe')
            ->setParameter('employe', $employe)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getEmployeReviews($employe)
    {
        return $this->createQueryBuilder('s')
            ->select('b.review')
            ->innerJoin('s.booking', 'b')
            ->innerJoin('s.employe', 'e')
            ->andWhere('s.employe = :employe')
            ->setParameter('employe', $employe)
            ->orderBy('s.endTime', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }


//    /**
//     * @return Session[] Returns an array of Session objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Session
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
