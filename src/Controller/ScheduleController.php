<?php


namespace App\Controller;


use App\Entity\Booking;
use App\Repository\BookingsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class ScheduleController extends Controller
{
    /**
     * @Route("/schedule", name="app-schedule")
     * @param BookingsRepository $bookingsRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showScheduleAction(BookingsRepository $bookingsRepository)
    {
        $bookings = $bookingsRepository->getSchedule();
        return $this->render('schedule.html.twig', [
            'bookings' => $bookings
        ]);
    }


}