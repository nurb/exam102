<?php


namespace App\Controller;


use App\Entity\Booking;
use App\Repository\BookingsRepository;
use App\Repository\EmployeesRepository;
use App\Repository\SessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class EmployeesController extends Controller
{
    /**
     * @Route("/employees", name="app-employees")
     * @param EmployeesRepository $employeesRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param BookingsRepository $bookingsRepository
     */
    public function showEmployeesAction(EmployeesRepository $employeesRepository)
    {
        $employees = $employeesRepository->findAll();
        return $this->render('employees.html.twig', [
            'employees' => $employees
        ]);
    }

    /**
     * @Route("/employe/{id}", name="app-employe")
     * @param int $id
     * @param EmployeesRepository $employeesRepository
     * @param SessionRepository $sessionRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showSingleEmployeAction(int $id, EmployeesRepository $employeesRepository, SessionRepository $sessionRepository)
    {
        $employe = $employeesRepository->find($id);
        $count = $sessionRepository->getEmployeWorks($employe);
        $reviews = $sessionRepository->getEmployeReviews($employe);


        dump($reviews);

        return $this->render('employe.html.twig', [
            'employe' => $employe,
            'count' => $count[1],
            'reviews' => $reviews
        ]);
    }


}