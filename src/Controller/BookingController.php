<?php


namespace App\Controller;


use App\Entity\Booking;
use App\Entity\Session;
use App\Form\BookingType;
use App\Repository\BookingsRepository;
use App\Repository\EmployeesRepository;
use App\Repository\SessionRepository;
use function Couchbase\defaultDecoder;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;


class BookingController extends Controller
{
    /**
     * @Route("/booking/add", name="app-booking-add")
     * @param Request $request
     * @param SessionRepository $sessionRepository
     * @param EmployeesRepository $employeesRepository
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param EmployeesRepository $employeesRepository
     */
    public function addBookingAction(Request $request, SessionRepository $sessionRepository, EmployeesRepository $employeesRepository, ObjectManager $objectManager)
    {
        if ($this->getUser()) {
            $booking = new Booking();
            $booking->setUser($this->getUser());
            $form = $this->createForm(BookingType::class, $booking, array(
                'method' => 'POST'
            ));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $workHours = $booking->getObjectSize() / 20 * $booking->getCleaningType();
                $amountOfDays = ceil($workHours / 8);
                $cleaningStartTime = new \DateTimeImmutable($booking->getBookingDate()->format('Y-m-d H:i:s'));
                $cleaningEndTime = $cleaningStartTime->modify("+{$amountOfDays} days");
                $employees = $employeesRepository->findAll();
                $lastKey = count($employees);
                foreach ($employees as $key => $employe) {
                    $sessionOnCurrentDate = $sessionRepository->getSessionByDateAndEmploye(
                        $cleaningStartTime,
                        $cleaningEndTime,
                        $employe
                    );
                    if ($sessionOnCurrentDate == null) {
                        $hoursUntilDayOff = 20 - (int)$cleaningStartTime->format('H');
                        while ($workHours > 0) {
                            $session = new Session();
                            $session->setEmploye($employe);
                            $session->setBooking($booking);
                            $session->setStartTime($cleaningStartTime);
                            if ($hoursUntilDayOff > 8) {
                                $session->setEndTime($cleaningStartTime->modify('+8 hours'));
                                $workHours -= 8;
                            } else {
                                $session->setEndTime($cleaningStartTime->modify("+{$hoursUntilDayOff} hours"));
                                $workHours -= $hoursUntilDayOff;

                            }
                            $cleaningStartTime = ($cleaningStartTime->modify("+1 days")->setTime(8, 00, 00));
                            $objectManager->persist($booking);
                            $objectManager->persist($session);
                            $objectManager->flush();
                            $hoursUntilDayOff = 9;
                        }
                        $this->addFlash('notice', 'Спасибо, ваше бронирование принято');
                        return $this->redirectToRoute("homepage");
                    } elseif ($lastKey == $key + 1) {
                        $this->addFlash('error', 'К сожалению, на эту все распродано');
                    }
                }

            }
            $showForm = $form->createView();
        } else {
            $showForm = null;
        }


        return $this->render('booking.html.twig', [
            'form' => $showForm
        ]);
    }


    /**
     * @Route("/booking/my", name="app-booking-show")
     * @param BookingsRepository $bookingsRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showMyBookingsAction(BookingsRepository $bookingsRepository)
    {
        $bookings = null;
        if ($this->getUser()) {
            /**
             * @var Booking
             */
            $bookings = $bookingsRepository->getBookingsByUser($this->getUser());
        }


        return $this->render('mybooking.html.twig', [
            'bookings' => $bookings
        ]);
    }

    /**
     * @Route("/booking/remove/{id}", name="app_booking_remove")
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeBookingAction(int $id, Request $request, BookingsRepository $bookingsRepository, ObjectManager $manager)
    {
        $booking = $bookingsRepository->find($id);
        if ($booking->getUser() == $this->getUser()) {
            $manager->remove($booking);
            $manager->flush();
            $this->addFlash('notice', "Бронирование успешно отменено");
        } else {
            $this->addFlash('error', "Ошибка аунтефикации");
        }


        return $this->redirect($request->headers->get('referer'));
    }


}