<?php


namespace App\DataFixtures;


use App\Entity\Employe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EmployeFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $employe1 = new Employe();
        $employe1->setName('Вася');
        $manager->persist($employe1);
        $this->addReference('employe1', $employe1);

        $employe2 = new Employe();
        $employe2->setName('Петя');
        $manager->persist($employe2);
        $this->addReference('employe2', $employe2);

        $employe3 = new Employe();
        $employe3->setName('Таня');
        $manager->persist($employe3);
        $this->addReference('employe3', $employe3);

        $manager->flush();
    }
}