<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BookingFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = $this->getReference('user');
        $booking1 = new Booking();
        $booking1->setAddress('Some street 1')
            ->setUser($user)
            ->setCleaningType('1')
            ->setObjectSize('100')
            ->setBookingDate(new \DateTime('2017-10-10 08:00:00'));
        $this->addReference('booking1', $booking1);
        $manager->persist($booking1);

        $booking2 = new Booking();
        $booking2->setAddress('Some street 2')
            ->setUser($user)
            ->setCleaningType('1')
            ->setObjectSize('100')
            ->setBookingDate(new \DateTime('2017-10-10 08:00:00'));
        $this->addReference('booking2', $booking2);
        $manager->persist($booking2);

        $booking3 = new Booking();
        $booking3->setAddress('Some street 3')
            ->setUser($user)
            ->setCleaningType('1')
            ->setObjectSize('100')
            ->setBookingDate(new \DateTime('2017-10-10 08:00:00'));
        $this->addReference('booking3', $booking3);
        $manager->persist($booking3);

        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}