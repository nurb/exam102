<?php


namespace App\DataFixtures;


use App\Entity\Booking;
use App\Entity\Session;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SessionFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $session1 = new Session();
        $session1->setEmploye($this->getReference('employe1'))
            ->setBooking($this->getReference('booking1'))
            ->setStartTime(new \DateTime('2017-10-10 08:00:00'))
            ->setEndTime(new \DateTime('2017-10-10 14:00:00'));
        $manager->persist($session1);

        $session2 = new Session();
        $session2->setEmploye($this->getReference('employe2'))
            ->setBooking($this->getReference('booking2'))
            ->setStartTime(new \DateTime('2017-10-10 08:00:00'))
            ->setEndTime(new \DateTime('2017-10-10 14:00:00'));
        $manager->persist($session2);

        $session3 = new Session();
        $session3->setEmploye($this->getReference('employe3'))
            ->setBooking($this->getReference('booking3'))
            ->setStartTime(new \DateTime('2017-10-10 08:00:00'))
            ->setEndTime(new \DateTime('2017-10-10 14:00:00'));
        $manager->persist($session3);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            EmployeFixtures::class,
            BookingFixtures::class,
        );
    }
}